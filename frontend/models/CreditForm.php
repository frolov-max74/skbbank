<?php

namespace frontend\models;

use common\models\Request;
use yii\base\Model;
use Yii;

/**
 * Class CreditForm
 * @package frontend\models
 */
class CreditForm extends Model
{
    public $last_name;
    public $first_name;
    public $middle_name;
    public $birth_date;
    public $phone;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['last_name', 'first_name', 'middle_name'], 'trim'],
            [['last_name', 'first_name', 'birth_date', 'phone'], 'required'],
            [['birth_date', 'phone'], 'string'],
            [['birth_date', 'phone'], 'match', 'pattern' => '/[\_]+/', 'not' => true],
            [['first_name', 'last_name', 'middle_name'], 'string', 'min' => 2, 'max' => 50],
            [
                ['last_name', 'first_name', 'middle_name'],
                'match',
                'pattern' => '/^[а-яё]+$/iu',
                'message' => 'Пожалуйста, указывайте только русские буквы.'
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'middle_name' => 'Отчество',
            'birth_date' => 'Дата рождения',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @return bool
     */
    public function createRequest(): bool
    {
        if (!$this->validate()) {
            return false;
        }
        $this->sanitizeAttributes();

        $request = new Request();
        $request->last_name = $this->last_name;
        $request->first_name = $this->first_name;
        $request->middle_name = $this->middle_name;
        $request->birth_date = $this->birth_date;
        $request->phone = $this->phone;
        $request->user_ip = Yii::$app->request->userIP;
        $request->user_agent = Yii::$app->request->userAgent;

        return $request->save() ? true : false;
    }

    /**
     * @param string $encoding
     */
    private function sanitizeAttributes($encoding = 'UTF-8')
    {
        foreach ($this->attributes as $name => $value) {
            if (in_array($name, ['last_name', 'first_name', 'middle_name'])) {
                $value = mb_strtolower($value, $encoding);
                $this->$name = mb_strtoupper(mb_substr($value, 0, 1, $encoding), $encoding) .
                    mb_substr($value, 1, null, $encoding);
            }
            if ($name == 'birth_date') {
                $this->$name = strtotime($value);
            }
            if ($name == 'phone') {
                $this->$name = preg_replace('/[\D]/', '', $value);
            }
        }
    }
}
