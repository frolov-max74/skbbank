<?php

namespace frontend\controllers;

use frontend\models\CreditForm;
use yii\web\Controller;
use Yii;

/**
 * Class CreditController
 * @package frontend\controllers
 */
class CreditController extends Controller
{
    public function actionRequest()
    {
        $model = new CreditForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->createRequest()) {
                $this->redirect(['thanks']);
            }
        }
        return $this->render('request', [
            'model' => $model,
        ]);
    }

    public function actionThanks()
    {
        return $this->render('thanks');
    }

}
