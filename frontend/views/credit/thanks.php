<?php

/* @var $this yii\web\View */

$this->title = 'Спасибо';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-thanks">
    <h1>Спасибо за обращение!</h1>
    <p>Ваша заявка принята в работу.</p>
</div>
