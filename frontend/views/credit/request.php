<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\CreditForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use aayaresko\date\MaskedInputDatePicker;

$this->title = 'Заявка на кредит';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="credit-request">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Оставьте заявку на кредит и узнайте решение Банка
    </p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'request-form']); ?>

            <?= $form->field($model, 'last_name')->textInput() ?>

            <?= $form->field($model, 'first_name')->textInput() ?>

            <?= $form->field($model, 'middle_name')->textInput() ?>

            <?= $form->field($model, 'birth_date')->widget(
                MaskedInputDatePicker::class, [
                'enableMaskedInput' => true,
                'maskedInputOptions' => [
                    'mask' => '99.99.9999',
                    'pluginEvents' => [
                        'complete' => "function(){console.log('complete');}"
                    ]
                ],
                'name' => 'dp_3',
                'type' => MaskedInputDatePicker::TYPE_COMPONENT_APPEND,
                'value' => '23.09.1982',
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd.M.yyyy'
                ]
            ]); ?>

            <?= $form->field($model, 'phone')->widget(MaskedInput::class, [
                'mask' => '+7(999)999-99-99',
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Получить кредит', [
                    'class' => 'btn btn-warning',
                    'name' => 'contact-button'
                ]) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
