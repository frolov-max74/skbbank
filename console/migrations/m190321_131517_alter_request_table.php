<?php

use yii\db\Migration;

/**
 * Class m190321_131517_alter_request_table
 */
class m190321_131517_alter_request_table extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%request}}', 'phone', 'string(11) NOT NULL');
    }

    public function down()
    {
        $this->alterColumn('{{%request}}', 'phone', 'integer NOT NULL');
    }
}
